Dependencies: SDL2, SDL2-image, SDL2-ttf, (libfreetype, libpng16)

Compile on Linux with `gcc -no-pie src/blockie.c -o blockie -lSDL2 -lSDL2_ttf -lSDL2_image`.
