// Copyright (c) 2018 Hagen Schnuch

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//#include<stdio.h>   /* input/output */
//#include<stdlib.h>  /* srand, rand */
//#include<string.h>  /* string modification */
//#include<unistd.h>  /* sleep */
/* included in SDL.h */

//#include<time.h>    /* time */

#include"SDL2/SDL.h"
#include"SDL2/SDL_ttf.h"
#include"SDL2/SDL_image.h"

#ifdef _WIN32
	int win32 = 1;
	#include<stdio.h>   /* input/output */
#elif linux
	int win32 = 0;
#endif

/* GLOBALS */

/* Game Options */
unsigned short SCREEN_WIDTH = 800;
unsigned short SCREEN_HEIGHT = 600;
char const SCREEN_TITLE[] = "blockie";

int const TICKS = 1000;
int const FPS = 60;
int const TEXT_UPDATE = 5;

int const MOVE_SPEED_MOD = 250;
int const MOVE_JUMP_MOD = 4;
int const MOVE_WATER_MOD = 2;

float const GRAVITATION = 9.81;


/* SDL */
SDL_Event event;
SDL_Window *window = NULL;
SDL_Renderer *renderer = NULL;

//SDL_Texture *bgTexture = NULL;
SDL_Texture *playerTexture = NULL;
SDL_Texture *tilemapTexture = NULL;
SDL_Texture *editFrameTexture = NULL;


/* PATHS */
//char const BG_TEXTURE_PATH[] = "data/bg.png";
char const PLAYER_TEXTURE_PATH[] = "data/player.png";
char const TILEMAP_TEXTURE_PATH[] = "data/tilemap.png";
char const MAP_FILE_PATH[] = "data/level.map";
char const EDITFRAME_TEXTURE_PATH[] = "data/edit-frame.png";

TTF_Font *font;
TTF_Font *font_big;
char const FONT_PATH[] = "data/font-Regular.ttf";
char const FONT_BIG_PATH[] = "data/font-Bold.ttf";


/* TEXTS */
char CONTINUE_TEXT[] = "- GAME PAUSED -";


/* PLAYER */
int cam_x, cam_y;
struct Orb {
    int texture_x[2];
    int texture_w[2];
    SDL_Rect texture_rect;

    SDL_Rect rect;
    float x,y;
    float vely;

    char view_dir;

    char move_left;
    char move_right;
    char move_jump;
} player;

/* MAP */
unsigned int map_height, map_width;
unsigned int map_cols, map_rows;        // map size in tiles (width|height / tile_size)

unsigned int MAP_COLS_MAX = 512;        // strange errors > 1000
unsigned int MAP_ROWS_MAX = 512;

/* TILEMAP */
char *tiles;
unsigned short const TILE_SIZE = 32;
int TILEMAP_WIDTH;              // width of tilemap in pixel (gets queried)
int TILEMAP_HEIGHT;
int TILEMAP_WIDTH_TILES;        // width of tilemap in tiles (pixel/tile_size)
int TILEMAP_HEIGHT_TILES;

/*  Using just pointer arithmetic atm
struct Tilemap {
    char name[16];
    int x,y;
} tilemap[3];
*/

/* TILES */
char const TILE_SKY = -1;               // 
char const TILE_BEDROCK = 0;            // 
char const TILE_DIRT = 1;               // 

char const TILE_GRASS = 2;              // 
char const TILE_WOOD = 3;               // 
char const TILE_STONE = 4;              // 

char const TILE_DIRT_BG = 5;
char const TILE_GRASS_BG = 6;
char const TILE_WOOD_BG = 7;
char const TILE_STONE_BG = 8;
char const TILE_WATER = 9;

/* edit tiles menu */
char const PLACABLE_TILES_COUNT = 3;    // dirt+grass = 1 placable tile
char const FIRST_PLACABLE_TILE = 2;
char const FIRST_BG_TILE = 5;
char active_tile = 0;
char place_bg = 0;


/* MISC */
char quit = 0;
char debug_mode = -1;
char edit_mode = -1;

SDL_Point mouse_pos;

char const ALIGN_LEFT = 0;
char const ALIGN_CENTER = 1;
char const ALIGN_RIGHT = 2;

char const EDIT_DELETE = 0;
char const EDIT_PLACE = 1;
/* GLOBALS */


/* FUNCTIONS */
SDL_Texture* loadTexture( const char *path );

void drawtext(TTF_Font *font, char alignment, char shaded, char text[], unsigned char r, unsigned char g, unsigned char b, int x, int y);
void presstocontinue();
void editBlocks(char edit_action);
void saveGame();
/* FUNCTIONS */

//♣♦♠♥

int main(int argc, char *argv[]) {

    /* ---------- INIT ---------- */

    if ( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
        printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
        return 1;
    }
    else {

        if ( TTF_Init() < 0 ) {
            printf( "TTF could not initialize! TTF_Error: %s\n", TTF_GetError() );
            return 1;
        }
        else {
            font = TTF_OpenFont (FONT_PATH, 20);
            if ( font == NULL ) {
                printf( "Font could not be loaded! TTF_Error: %s\n", TTF_GetError() );
                return 1;
            }
            font_big = TTF_OpenFont (FONT_BIG_PATH, 20);
            if ( font == NULL ) {
                printf( "Font could not be loaded! TTF_Error: %s\n", TTF_GetError() );
                return 1;
            }
        }

        window = SDL_CreateWindow(  SCREEN_TITLE, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                    SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN|SDL_WINDOW_RESIZABLE );
        if ( window == NULL ) {
            printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
            return 1;
        }
        else {

            renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED );
            if ( renderer == NULL ) {
                printf( "Renderer could not be created! SDL_Error: %s\n", SDL_GetError() );
                return 1;
            }
            else {
                //SDL_SetRenderDrawColor( renderer, 0x33, 0x33, 0x33, 0xFF );   /* good old grey */
                SDL_SetRenderDrawColor( renderer, 0x88, 0xCC, 0xFF, 0xFF );     /* sky blue */
                SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "2" );  /* quality used for ie. rotation, 0 - worst, 1 - linear filter, 2- anisotropic */

                if ( !(IMG_Init( IMG_INIT_PNG )) ){
                    printf( "SDL_image could not initialize! IMG_Error: %s\n", IMG_GetError() );
                    return 1;
                }
            }
        }
    }


    //srand(time(NULL));
    /* ---------- INIT END ---------- */


    /* ----- load textures ----- */
    //bgTexture = loadTexture( BG_TEXTURE_PATH );
    tilemapTexture = loadTexture( TILEMAP_TEXTURE_PATH );
    editFrameTexture = loadTexture( EDITFRAME_TEXTURE_PATH );
    /* ----- load textures ----- */


    /* ----- Icon ----- */
    //♣♥♠♦
    {
        //SDL_Color color = { 255,0,0 };
        SDL_Color color = { 250,250,250 };
        SDL_Surface *surf_icon = NULL;
        //if(win32) surf_icon = IMG_Load( ICON_PATH );

        //surf_icon = IMG_Load( player.TEXTURE_PATH );

        //surf_icon = TTF_RenderUTF8_Blended(font, "♣", color);
        surf_icon = TTF_RenderUTF8_Blended(font, ";..;", color);

        SDL_SetWindowIcon(window, surf_icon);
        SDL_FreeSurface(surf_icon);
    }
    /* ----- Icon ----- */


    /* ----- Tilemap Init ----- */
    SDL_QueryTexture( tilemapTexture, NULL, NULL, &TILEMAP_WIDTH, &TILEMAP_HEIGHT );    // get tilemap width and height
    TILEMAP_WIDTH_TILES = TILEMAP_WIDTH / TILE_SIZE;
    TILEMAP_HEIGHT_TILES = TILEMAP_HEIGHT / TILE_SIZE;
    /* ----- Tilemap Init ----- */


    /* ----- Map Init ----- */
    SDL_RWops *map_file = SDL_RWFromFile( MAP_FILE_PATH, "r" );
    if( map_file ){

        int map_size = SDL_RWsize( map_file );
        char *map_string = (char*)malloc( map_size * sizeof(*map_string) +1 );   // sizeof(*map_string = char) should be 1
        tiles = (char*)calloc( map_size, sizeof(*tiles) );

        SDL_RWread( map_file, map_string, map_size, 1 );
        SDL_RWclose( map_file );
        map_string[map_size] = '\0';

        int cols_temp = 0, counter = 0;
        
        for( int k=0; k<map_size; k++ ){

            if( map_string[k] == '\n' ){
                map_rows++;
                if( cols_temp > map_cols ) map_cols = cols_temp;
                cols_temp = 0;
            }
            else {
                *(tiles + counter) = map_string[k] - '/' ;
                counter++;
                cols_temp++;
            }
        }
        tiles = realloc( tiles, map_rows*map_cols * sizeof(*tiles) );

        free(map_string); map_string = NULL;
    }
    /* --- create new map --- */
    else {
        int entries = MAP_COLS_MAX * MAP_ROWS_MAX;
        map_cols = MAP_COLS_MAX;
        map_rows = MAP_ROWS_MAX;
        tiles = (char*)malloc( sizeof( *tiles )* entries );
        for( int i=0; i < entries; i++ ){
            if( i < map_cols * MAP_ROWS_MAX/2 ) tiles[i] = TILE_SKY;           // place dirt from the half of max rows on
            else if ( i < map_cols * (MAP_ROWS_MAX/2 +1) ) tiles[i] = TILE_GRASS;
            else if ( i < map_cols * (MAP_ROWS_MAX-1) ) tiles[i] = TILE_DIRT;
            else tiles[i] = TILE_BEDROCK;
        }
    }

    /* check if map was created right
    for( int m=0; m<map_rows; m++ ){
        for( int n=0; n<map_cols; n++ ){
            printf( "%d", *(tiles + map_cols*m +n)+1 );
        }
        printf( "\n" );
    }  */


    /* Set map size */
    map_width = map_cols * TILE_SIZE;
    map_height = map_rows * TILE_SIZE;


    SDL_SetWindowMaximumSize( window, map_width, map_height );     // window shouldn't be resized bigger than the map

    /* change window size if the map is too small */
    if( map_width < SCREEN_WIDTH ) SCREEN_WIDTH = map_width;
    if( map_height < SCREEN_HEIGHT ) SCREEN_HEIGHT = map_height;
    SDL_SetWindowSize( window, SCREEN_WIDTH, SCREEN_HEIGHT );

    /* ----- Map Init ----- */


    /* ----- Player Init ----- */
    playerTexture = loadTexture( PLAYER_TEXTURE_PATH );

    player.texture_x[0] = 0;
    player.texture_w[0] = 16;
    player.texture_x[1] = 17;
    player.texture_w[1] = 24;
    player.texture_rect.x = 0;
    player.texture_rect.y = 0;
    player.texture_rect.w = 16;
    player.texture_rect.h = 24;

    player.rect.w = 16;
    player.rect.h = 24;
    player.x = map_width /2;   // spawn player in the middle of the map
    player.y = map_height /2;
    player.rect.x = player.x - player.rect.w / 2;
    player.rect.y = player.y - player.rect.h / 2;

    player.vely = 0;
    player.view_dir = 0;

    player.move_left = 0;
    player.move_right = 0;
    player.move_jump = 0;

    /* camera placement */
    if( player.x < SCREEN_WIDTH/2 || map_width <= SCREEN_WIDTH ) cam_x = 0;
    else if( map_width - player.x < SCREEN_WIDTH/2 ) cam_x = map_width - SCREEN_WIDTH;
    else cam_x = player.x - SCREEN_WIDTH/2;

    if( player.y < SCREEN_HEIGHT/2 || map_height <= SCREEN_HEIGHT ) cam_y = 0;
    else if( map_height - player.y < SCREEN_HEIGHT/2 ) cam_y = map_height - SCREEN_HEIGHT;
    else cam_y = player.y - SCREEN_HEIGHT/2;

    /* ----- Player Init ----- */



    /* ----- GAME LOOP ----- */
    int timer = 0, dtimer = 0, fps_timer = 0, text_timer = 0;
    char ticks_text[16], fps_text[16];

    char edit_place = 0, edit_delete = 0;

    while ( !quit ) {

        timer = SDL_GetTicks();

        if ( timer - dtimer < 1000.0/TICKS ) {
            SDL_Delay( 1000.0/TICKS -(timer-dtimer) );
            timer = SDL_GetTicks();
        }

        /* -----Events ----- */
        while ( SDL_PollEvent(&event)) {
        
            if ( event.type == SDL_QUIT) quit = 1;
            else if ( event.type == SDL_KEYDOWN ) {
                
                switch ( event.key.keysym.sym ) {
                
                    case SDLK_ESCAPE: quit = 1; break;

                    case SDLK_p: presstocontinue();
                                timer = SDL_GetTicks();         /* reset timer after pause or gravitation flips out */
                                dtimer = timer -1; break;

                    case SDLK_F1: debug_mode *= -1; break;
                    case SDLK_TAB: edit_mode *= -1; break;

                    /* --- Moving --- */
                    case SDLK_UP: player.move_jump = 1; break;
                    case SDLK_LEFT: player.move_left = 1; break;
                    case SDLK_RIGHT: player.move_right = 1; break;
                    case SDLK_w: player.move_jump = 1; break;
                    case SDLK_a: player.move_left = 1; break;
                    case SDLK_d: player.move_right = 1; break;
                    case SDLK_SPACE: player.move_jump = 1; break;
                    /* --- Moving --- */

                    case SDLK_LCTRL: place_bg = 1; break;
                }
            }
            else if ( event.type == SDL_KEYUP ) {
                switch ( event.key.keysym.sym ) {
                    /* --- Moving --- */
                    case SDLK_UP: player.move_jump = 0; break;
                    case SDLK_LEFT: player.move_left = 0; break;
                    case SDLK_RIGHT: player.move_right = 0; break;
                    case SDLK_w: player.move_jump = 0; break;
                    case SDLK_a: player.move_left = 0; break;
                    case SDLK_d: player.move_right = 0; break;
                    case SDLK_SPACE: player.move_jump = 0; break;
                    /* --- Moving --- */

                    case SDLK_LCTRL: place_bg = 0; break;
                }
            }
            else if( event.type == SDL_MOUSEBUTTONDOWN ){
                if( edit_mode ){
                    switch( event.button.button ){
                        case SDL_BUTTON_LEFT: edit_place = 1; break;
                        case SDL_BUTTON_RIGHT: edit_delete = 1; break;
                    }
                }
            }
            else if( event.type == SDL_MOUSEBUTTONUP ){
                if( edit_mode ){
                    switch( event.button.button ){
                        case SDL_BUTTON_LEFT: edit_place = 0; break;
                        case SDL_BUTTON_RIGHT: edit_delete = 0; break;
                    }
                }
            }
            else if( event.type == SDL_MOUSEWHEEL ){
                if( edit_mode ){
                    if( event.wheel.y > 0 ){
                        active_tile -= 1;
                        if( active_tile < 0 ) active_tile = PLACABLE_TILES_COUNT -1;
                    }
                    if( event.wheel.y < 0 ){
                        active_tile += 1;
                        if( active_tile >= PLACABLE_TILES_COUNT ) active_tile = 0;
                    }
                }
            }
            /* change screen variables if window was resized */
            else if( event.type == SDL_WINDOWEVENT ){
                switch( event.window.event ){
                    case SDL_WINDOWEVENT_SIZE_CHANGED: {
                        SCREEN_WIDTH = event.window.data1;
                        SCREEN_HEIGHT = event.window.data2;
                        if( SCREEN_WIDTH > map_width ) cam_x = 0;                                               // replace camera if resizing out 
                        else if( map_width - player.x < SCREEN_WIDTH/2 ) cam_x = map_width - SCREEN_WIDTH;      // replace camera if resizing in
                        if( SCREEN_HEIGHT > map_height ) cam_y = 0; 
                        else if( map_height - player.y < SCREEN_HEIGHT/2 ) cam_y = map_height - SCREEN_HEIGHT;
                        break;
                    }

                }

            }

        }
        if( quit ) break;
        /* ----- Events ----- */


        /* ----- Block Edits ----- */
        if( edit_place ) editBlocks(EDIT_PLACE);
        else if( edit_delete) editBlocks(EDIT_DELETE);
        /* ----- Block Edits ----- */


        /* ----- Physics ---- */
       
        {   /* --- Moving --- */
            float dt = ( timer - dtimer ) / 1000.0;     /* time passed since last call in seconds */

            int player_over_tile_x = player.x / TILE_SIZE;      /* coordinates of the tile behind the player */
            int player_over_tile_y = player.y / TILE_SIZE;
 
            char *tiles_player = tiles +(map_cols*player_over_tile_y)+ player_over_tile_x;
            char tile_behind_player = *tiles_player;
            char tile_below_player = *(tiles_player + map_cols);
            char tile_above_player = *(tiles_player - map_cols);

            int player_move_dir = 0;

            player.rect.w = player.texture_w[0];            // player width when standing
            player.texture_rect.x = player.texture_x[0];
            player.texture_rect.w = player.texture_w[0];
            
            float player_left = player.x - player.rect.w/2;
            float player_right = player.x + player.rect.w/2;
            float player_top = player.y - player.rect.h/2;
            float player_bottom = player.y + player.rect.h/2;

            int move_speed_mod = MOVE_SPEED_MOD;
            if( tile_behind_player == TILE_WATER ) move_speed_mod /= MOVE_WATER_MOD;

            /* --- Move Right/Left --- */

            if( player.move_left ){
                if( player_left > 0 ){
                    // if tile left of player is sky or background tile
                    if( *(tiles_player-1) < 0 || *(tiles_player-1) >= FIRST_BG_TILE ){
                        player_move_dir = -1;
                        player.view_dir = -1;
                    } else {
                        // collision if tile left of player is impassable
                        if( player_left > player_over_tile_x * TILE_SIZE ){
                            player_move_dir = -1;
                            player.view_dir = -1;
                        }
                    }
                }
            }
            else if( player.move_right ){
                if( player_right < map_cols * TILE_SIZE ){
                    // if tile right of player is sky or background tile
                    if( *(tiles_player+1) < 0 || *(tiles_player+1) >= FIRST_BG_TILE ){
                        player_move_dir = +1;
                        player.view_dir = +1;
                    } else {
                        // collision if tile right of player is impassable
                        if( player_right < (player_over_tile_x+1) * TILE_SIZE ){
                            player_move_dir = +1;
                            player.view_dir = +1;
                        }
                    }
                }
            }

            // apply moving
            if( player_move_dir ){
                player.x += player_move_dir * move_speed_mod *dt;
                //player.rect.x = player.x - player.rect.w / 2;

                // change width to render moving player correctly
                player.rect.w = player.texture_w[1];
                player.texture_rect.x = player.texture_x[1];
                player.texture_rect.w = player.texture_w[1];

            }
            /* --- Move Right/Left --- */


            /* --- Falling/Jumping --- */
            int player_in_air = 0;

            if( tile_behind_player >= 0 && tile_behind_player < FIRST_BG_TILE ){      // if we fell into a block somehow, set us above it
                player.y = (player_over_tile_y) * TILE_SIZE - player.rect.h/2;
                player.vely = 0;
            }
            else{
                if( tile_below_player >= 0 && tile_below_player < FIRST_BG_TILE ){     // is there ground below player?
                    if( player_bottom < (player_over_tile_y +1) * TILE_SIZE ){          // player not standing on the ground?
                        player_in_air = 1;
                    } else if( player_bottom > (player_over_tile_y +1) * TILE_SIZE ){   // fell slightly into the ground
                        player.y = (player_over_tile_y+1) * TILE_SIZE - player.rect.h/2;
                        player.vely = 0;
                    } else {                       // player standing on the ground
                        if( player.move_jump ){
                            player.vely = -GRAVITATION/MOVE_JUMP_MOD;
                        }
                    }
                }
                else{
                    player_in_air = 1;
                }
            }
            if( player_in_air ){
                if( player.vely < 0 ){
                    if( tiles_player - map_cols > tiles ){          // can jump out off the top of the screen
                        if( tile_above_player >= 0 && tile_above_player < FIRST_BG_TILE ){               // jumping against a block?
                            if( player_top < player_over_tile_y * TILE_SIZE ){
                                player.vely = 0;
                                player.y = player_over_tile_y * TILE_SIZE + player.rect.h/2;
                            }
                        }
                    }
                }
                player.vely += GRAVITATION *dt;
            }

            player.y += player.vely * (move_speed_mod) * dt;
            /* --- Falling/Jumping --- */

            /* camera placement */
            if( player.x > SCREEN_WIDTH/2 && player.x < map_width - SCREEN_WIDTH/2) cam_x = player.x - SCREEN_WIDTH/2;
            if( player.y > SCREEN_HEIGHT/2 && player.y < map_height - SCREEN_HEIGHT/2) cam_y = player.y - SCREEN_HEIGHT/2;

            player.rect.x = player.x - cam_x -player.rect.w/2;
            player.rect.y = player.y - cam_y -player.rect.h/2;


        }   /* --- Moving --- */
        
        /* ----- Physics ---- */


        /* ----- Render ----- */
        if( timer - fps_timer > 1000.0/FPS ) {

            SDL_RenderClear(renderer);

            /* --- draw tiles --- */
            int draw_height, draw_width;        /* don't render, what's not in the map */
            if( cam_x + SCREEN_WIDTH > cam_x + map_width ) draw_width = cam_x + map_width;
            else draw_width = cam_x + SCREEN_WIDTH + TILE_SIZE;          // +TILE_SIZE : render one more tile for a smooth transition
            if( cam_y + SCREEN_HEIGHT > cam_y + map_height ) draw_height = cam_y + map_height;
            else draw_height = cam_y + SCREEN_HEIGHT + TILE_SIZE;

            for( int i=cam_y/TILE_SIZE; i< draw_height/TILE_SIZE; i++ ){
                for( int j=cam_x/TILE_SIZE; j< draw_width/TILE_SIZE; j++ ){
                    int tile = *(tiles +(map_cols*i)+ j );
                    if( tile < 0 ) continue;   // don't render sky (0:Bedrock, -1:Sky) - earlier -> ??? : (-1: Bedrock, -2:Sky)
                    //SDL_Rect tileRect = { tile*TILE_SIZE,0, TILE_SIZE,TILE_SIZE };        // use proper arithmetic to point to tiles in the tilemap
                    SDL_Rect tileRect = { (tile%TILEMAP_WIDTH_TILES)*TILE_SIZE, (tile/TILEMAP_WIDTH_TILES)*TILE_SIZE, TILE_SIZE,TILE_SIZE };
                    SDL_Rect renderRect = { j*TILE_SIZE-cam_x, i*TILE_SIZE-cam_y, TILE_SIZE,TILE_SIZE };
                    SDL_RenderCopy( renderer, tilemapTexture, &tileRect, &renderRect );
                }
            }

            /* --- draw tiles --- */


            /* --- Update Text --- */
            if( timer - text_timer > 1000.0/TEXT_UPDATE ) {

                if( debug_mode > 0 ){
                    sprintf( ticks_text, "Ticks: %.2f", 1000.0/( timer - dtimer ) );
                    sprintf( fps_text, "FPS: %.2f", 1000.0/( timer - fps_timer ) );

                }

            text_timer = timer;
            }
            /* --- Update Text --- */

            /* draw text */
            if( debug_mode > 0 ){
                drawtext( font, 0, 1, fps_text, 255,255,255, 10,10 );
                drawtext( font, 0, 1, ticks_text, 255,255,255, 10,30 );
            }


            /* --- draw other stuff --- */
            if( player.view_dir < 0 ){
                SDL_RenderCopyEx( renderer, playerTexture, &player.texture_rect, &player.rect, 0, NULL, SDL_FLIP_HORIZONTAL );
            }
            else{
                SDL_RenderCopyEx( renderer, playerTexture, &player.texture_rect, &player.rect, 0, NULL, SDL_FLIP_NONE );
            }


            /* draw edit frame & tile menu */
            if( edit_mode > 0 ) {

                SDL_GetMouseState( &mouse_pos.x, &mouse_pos.y );
                if( mouse_pos.x < map_width -cam_x  && mouse_pos.y < map_height -cam_y ) {     // don't draw outside of map
                    mouse_pos.x = (mouse_pos.x + cam_x) / TILE_SIZE;    // get tilemap position of the tile the mouse is pointing on
                    mouse_pos.x = mouse_pos.x * TILE_SIZE - cam_x;      // get screen position of said tile depending on the camera
                    mouse_pos.y = (mouse_pos.y + cam_y) / TILE_SIZE;
                    mouse_pos.y = mouse_pos.y * TILE_SIZE - cam_y;
                    SDL_Rect editFrameRect = { mouse_pos.x,mouse_pos.y, TILE_SIZE,TILE_SIZE };
                    SDL_RenderCopy( renderer, editFrameTexture, NULL, &editFrameRect );
                }

                {
                    int tile_menu_x, tile_menu_y;
                    tile_menu_x = SCREEN_WIDTH /2 - PLACABLE_TILES_COUNT * TILE_SIZE /2;
                    tile_menu_y = SCREEN_HEIGHT - TILE_SIZE;

                    int tile = FIRST_PLACABLE_TILE;

                    SDL_Rect tileMenuRect = { tile_menu_x,tile_menu_y, TILE_SIZE,TILE_SIZE };

                    for( int i=0; i < PLACABLE_TILES_COUNT; i++ ){
                        SDL_Rect tileRect = { (tile%TILEMAP_WIDTH_TILES)*TILE_SIZE, (tile/TILEMAP_WIDTH_TILES)*TILE_SIZE, TILE_SIZE,TILE_SIZE };
                        SDL_RenderCopy( renderer, tilemapTexture, &tileRect, &tileMenuRect );
                        tile += 1;
                        tileMenuRect.x += TILE_SIZE;
                    }

                    SDL_Rect activeTileRect = { tile_menu_x + active_tile*TILE_SIZE, tile_menu_y, TILE_SIZE,TILE_SIZE };
                    SDL_RenderCopy( renderer, editFrameTexture, NULL, &activeTileRect );
                }
            }
            /* --- draw other stuff --- */


            SDL_RenderPresent(renderer);

            fps_timer = timer;
        }
        /* ----- Render ----- */

        dtimer = timer;


    }
    /* ----- GAME LOOP ----- */


    /* Save */
    saveGame();


    /* ----- free memory ---- */
    free(tiles); tiles = NULL;

    //SDL_DestroyTexture(bgTexture);
    SDL_DestroyTexture(playerTexture);
    SDL_DestroyTexture(tilemapTexture);
    SDL_DestroyTexture(editFrameTexture);

    SDL_DestroyRenderer(renderer);

    SDL_DestroyWindow(window);

    TTF_CloseFont(font);
    TTF_CloseFont(font_big);

    TTF_Quit();
    IMG_Quit();
    SDL_Quit();

    return 0;
}


void drawtext(TTF_Font *font, char alignment, char shaded, char text[], unsigned char r, unsigned char g, unsigned char b, int x, int y) {
    int w, h;
    SDL_Color color = { r, g, b };
    TTF_SizeUTF8( font, text, &w, &h );
    if( alignment == 1 ) x -= w/2;
    if( alignment == 2 ) x -= w;
    SDL_Rect rect = { x, y, w, h };

    if( shaded ){
        SDL_Rect rect_shaded = { x-1, y+1, w, h }; 
        SDL_Rect rect_together = { x-1, y, w, h+1 }; 

        SDL_Color color_shaded = { 0,0,0 };
        SDL_Surface *textsur_shaded = TTF_RenderUTF8_Blended(font, text, color_shaded);
        SDL_Texture *texture_shaded = SDL_CreateTextureFromSurface(renderer, textsur_shaded);
        SDL_SetTextureAlphaMod(texture_shaded, 100);
        SDL_RenderCopy( renderer, texture_shaded, NULL, &rect_shaded );

        SDL_DestroyTexture(texture_shaded);
        SDL_FreeSurface(textsur_shaded);
    }
    
    SDL_Surface *textsur = TTF_RenderUTF8_Blended(font, text, color);
    SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, textsur);
    SDL_RenderCopy(renderer, texture, NULL, &rect);

    SDL_DestroyTexture(texture);
    SDL_FreeSurface(textsur);
}


SDL_Texture* loadTexture( const char *path ) {

    //The final texture
    SDL_Texture* newTexture = NULL;

    //Load image at specified path
    SDL_Surface* loadedSurface = IMG_Load( path );
    if( !loadedSurface ) { 
        printf( "Unable to load image %s! SDL_image Error: %s\n", path, IMG_GetError() );
    } 
    else { 
        //Create texture from surface pixels
        newTexture = SDL_CreateTextureFromSurface( renderer, loadedSurface );
        if( !newTexture ) { 
            printf( "Unable to create texture from %s! SDL Error: %s\n", path, SDL_GetError() );
        } 

        //Get rid of old loaded surface 
        SDL_FreeSurface( loadedSurface ); 
    } 
    return newTexture; 
}


void presstocontinue(){

    player.move_left = 0;
    player.move_right = 0;
    player.move_jump = 0;

    //drawtext( font, 1, CONTINUE_TEXT, 255,255,255, 20, SCREEN_HEIGHT-25 );
    drawtext( font_big, ALIGN_CENTER, 1, CONTINUE_TEXT, 255,255,255, SCREEN_WIDTH/2, SCREEN_HEIGHT/2 );
    SDL_RenderPresent(renderer);

    /* Eingabeabfrage */
    while ( SDL_WaitEvent(&event) ) {
        SDL_FlushEvent(SDL_KEYDOWN);    // Flush unwanted events from queue
        SDL_FlushEvent(SDL_KEYUP);

    
        if ( event.type == SDL_QUIT) quit = 1;
        else if ( event.type == SDL_KEYDOWN ) {
            
            switch ( event.key.keysym.sym ) {
                                   
                case SDLK_ESCAPE: 
                    quit = 1;
                break;

                case SDLK_p: break;

                default:
                    continue;       /* Bei falscher Eingabe, gehe an anfang der Schleife */

            }
        }
        else continue;
        break;
    }
//    SDL_RenderClear(renderer);
//    SDL_RenderCopy( renderer, bgTexture, NULL, NULL );
//    SDL_RenderPresent(renderer);
}


void editBlocks(char edit_action){
    
    if( edit_mode > 0 ){
        SDL_Point mouse_pos;
        SDL_GetMouseState( &mouse_pos.x, &mouse_pos.y );

        /* don't edit outside of the map */
        if( mouse_pos.x < map_width -cam_x  && mouse_pos.y < map_height -cam_y ) {

            mouse_pos.x = (mouse_pos.x + cam_x) / TILE_SIZE;
            mouse_pos.y = (mouse_pos.y + cam_y) / TILE_SIZE;

            /* don't edit at the players position */
            if( mouse_pos.x != (int)player.x/TILE_SIZE || mouse_pos.y != (int)player.y/TILE_SIZE ){

                char *tile = tiles +(map_cols*mouse_pos.y)+ mouse_pos.x;

                if( edit_action == EDIT_PLACE ){
                    //if( *tile != TILE_BEDROCK && *tile != TILE_GRASS ){
                    if( *tile == TILE_SKY ){
                        if( !place_bg ){
                            if( active_tile + FIRST_PLACABLE_TILE ==  TILE_GRASS ){
                                if( *(tile-map_cols) > 0 || tile-tiles < map_cols ) *tile = TILE_DIRT;  // if tile is below grass or the "roof" (first row)
                                else *tile = TILE_GRASS;
                            } else {
                                *tile = active_tile + FIRST_PLACABLE_TILE;
                            }
                            if( *(tile+map_cols) == TILE_GRASS ) *(tile+map_cols) = TILE_DIRT;
                            if( *(tile+map_cols) == TILE_GRASS_BG ) *(tile+map_cols) = TILE_DIRT_BG;
                        } else {
                            if( active_tile + FIRST_PLACABLE_TILE ==  TILE_GRASS ){
                                if( *(tile-map_cols) > 0 || tile-tiles < map_cols ) *tile = TILE_DIRT_BG;  // if tile is below grass or the "roof" (first row)
                                else *tile = TILE_GRASS_BG;
                            } else {
                                *tile = active_tile + FIRST_BG_TILE +1;     // +1 for first bg tile being dirt
                            }
                            if( *(tile+map_cols) == TILE_GRASS_BG ) *(tile+map_cols) = TILE_DIRT_BG;
                        }
                    }
                }
                if( edit_action == EDIT_DELETE ){
                    if( *tile > 0 ) *tile = TILE_SKY;
                    if( *(tile+map_cols) == TILE_DIRT ) *(tile+map_cols) = TILE_GRASS;
                }
            }
        }
    }
}


void saveGame() {

    SDL_RWops *map_file = SDL_RWFromFile( MAP_FILE_PATH, "w" );
    if( map_file ){
        char map_string[map_rows*map_cols + map_rows];                                              // +map_rows for the extra '\n' on each line
        for( int row=0; row < map_rows; row++ ){
            for( int col=0; col < map_cols; col++ ){
                *(map_string + (row*map_cols) +col +row) = *(tiles + (row*map_cols) +col) + '/';    // +row for the extra '\n' on each line
            }
            *(map_string + ((row+1)*map_cols) +row)  = '\n';                                        // +row for the extra '\n' on each line
        }
        *(map_string + map_rows*map_cols +map_rows) = '\0';                                         // +map_rows for the extra '\n' on each line
        int str_len = SDL_strlen(map_string);
        if( SDL_RWwrite( map_file, map_string, 1, str_len ) != str_len ) printf("Saving failed...\n");
        else printf( "Map saved!\n");
        SDL_RWclose( map_file );
    }
}
